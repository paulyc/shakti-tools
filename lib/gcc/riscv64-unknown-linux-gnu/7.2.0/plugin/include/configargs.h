/* Generated automatically. */
static const char configuration_arguments[] = "/home/sathya/riscv-tools/riscv-gnu-toolchain/riscv-gcc/configure --target=riscv64-unknown-linux-gnu --prefix=/media/sathya/secondary_drive/tools --with-sysroot=/media/sathya/secondary_drive/tools/sysroot --with-system-zlib --enable-shared --enable-tls --enable-languages=c,c++,fortran --disable-libmudflap --disable-libssp --disable-libquadmath --disable-nls --disable-bootstrap --src=.././riscv-gcc --enable-checking=yes --disable-multilib --with-abi=lp64d --with-arch=rv64imafdc";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "abi", "lp64d" }, { "arch", "rv64imafdc" } };
