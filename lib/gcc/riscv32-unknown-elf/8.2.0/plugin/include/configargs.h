/* Generated automatically. */
static const char configuration_arguments[] = "/media/sathya/secondary_drive/riscv-tools/riscv-gnu-toolchain/build/../riscv-gcc/configure --target=riscv32-unknown-elf --prefix=/media/sathya/secondary_drive/tools --disable-shared --disable-threads --enable-languages=c,c++ --with-system-zlib --enable-tls --with-newlib --with-sysroot=/media/sathya/secondary_drive/tools/riscv32-unknown-elf --with-native-system-header-dir=/include --disable-libmudflap --disable-libssp --disable-libquadmath --disable-libgomp --disable-nls --src=../../riscv-gcc --enable-checking=yes --disable-multilib --with-abi=ilp32 --with-arch=rv32imac --with-tune=rocket 'CFLAGS_FOR_TARGET=-Os  -mcmodel=medlow' 'CXXFLAGS_FOR_TARGET=-Os  -mcmodel=medlow'";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "abi", "ilp32" }, { "arch", "rv32imac" }, { "tune", "rocket" } };
