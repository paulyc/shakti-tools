This repository hosts a set of toolchain executables that support shakti-sdk. 
[shakti-sdk](https://gitlab.com/shaktiproject/software/shakti-sdk) is a software development kit, that enables developers to work on [shakti core](https://gitlab.com/shaktiproject/cores).
For ease of use, the toolchain executables were generated and hosted in this repository.

The tool chain executables were generated from [riscv-tools](https://gitlab.com/shaktiproject/software/riscv-tools).

Ubuntu 16.04 used for generating the toolchain executables.

List of executables:

1. riscv gcc compiler for riscv 64 bit.
2. riscv gcc compiler for riscv 32 bit.
 

### Download toolchain

```
system:~$ git clone https://gitlab.com/shaktiproject/software/shakti-tools.git"
system:~$ cd shakti-tools
```

### Export tool chain

Export the tool chain to the `PATH` variable. This will help in using the toolchain everywhere in linux.

```
system:~$ SHAKTITOOLS=/PATH/OF/TOOL/
system:~$ export PATH=$PATH:$SHAKTITOOLS/bin
```

Things to do

* Please put the above line in .bashrc in home folder.

* The $SHAKTITOOLS is the location of the repository. 

* The command *which riscv64-unknown-elf-gcc* helps you to verify the export of toolchain.

Example:

```
system:~$ git clone https://gitlab.com/shaktiproject/software/shakti-tools.git
system:~$ pwd
/home/user
system:~$ cd shakti-tools
system:~$ SHAKTITOOL=/home/user/shakti-tools
system:~$ export PATH=$PATH:$SHAKTITOOL/bin
system:~$ which riscv64-unknown-elf-gcc
**/home/user/shakti-tools/bin**
system:~$ 
```

Please raise issues using [issues](https://gitlab.com/shaktiproject/software/shakti-tools/issues) menu.
